import React, { useState, useEffect } from "react";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles/index';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Link } from "react-router-dom";

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import red from '@material-ui/core/colors/red';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const styles = theme => ({
    root: {
        flexGrow: 1,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
    },
    toolbar: theme.mixins.toolbar,
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
        color: '#ffffff'
    },
    card: {
        maxWidth: 1024,
        margin: '0 auto',
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    actions: {
        display: 'flex',
    },
    avatar: {
        backgroundColor: red[500],
    },
});

const Detail = (props) => {
    const { classes, match } = props;
    const [initialized, setInitialized] = useState(false);
    const [beer, setBeer] = useState();
    const [favorited, setFavorited] = useState();

    useEffect(() => {
        if (!initialized) {
            const apiKey = 'bbd8e9f800b52500d7a3e1528c032250';
            fetch(`/v2/beer/${props.match.params.id}/?key=${apiKey}`)
                .then((response) => response.json())
                .then((responseJson) => {
                    setBeer(responseJson.data)
                    let favorites = localStorage.getItem('favorites') ? JSON.parse(localStorage.getItem('favorites')) : [];
                    setFavorited(favorites.indexOf(responseJson.data.id) >= 0);
                })
            setInitialized(true);
        }
    }, []);

    const favorite = () => {
        let favorites = localStorage.getItem('favorites') ? JSON.parse(localStorage.getItem('favorites')) : [];
        let isFavorited = favorites.indexOf(beer.id) >= 0;

        if(isFavorited) {
            favorites.splice(favorites.indexOf(beer.id), 1);
        } else if(favorites.length < 10) {
            favorites.push(beer.id);
        } else {
            isFavorited = true;
        }

        localStorage.setItem('favorites', JSON.stringify(favorites));
        setFavorited(!isFavorited);
    }

    return (
        <div className={classes.root}>
            <AppBar position="absolute" className={classes.appBar}>
                <Toolbar>
                    <Link to={'/'}>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                            <ArrowBackIcon />
                        </IconButton>
                    </Link>
                    <Typography variant="title" color="inherit" noWrap>
                        Beers Showroom
                    </Typography>
                </Toolbar>
            </AppBar>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                {beer ?
                    <Card className={classes.card}>
                        <CardHeader
                            avatar={
                                <Avatar aria-label="Recipe" className={classes.avatar}>
                                    B
                                </Avatar>
                            }
                            action={
                                <IconButton>
                                    <MoreVertIcon />
                                </IconButton>
                            }
                            title={beer.name}
                            subheader={beer.style ? beer.style.name : ''}
                        />
                        <CardMedia
                            className={classes.media}
                            image={beer.labels ? beer.labels.medium : '/no-thumbnail.png'}
                            title={beer.name}
                        />
                        <CardContent>
                        <Typography component="p">
                                <b>Organic: {beer.isOrganic}</b>
                            </Typography>
                            <Typography component="p">
                                {beer.description}
                            </Typography>
                        </CardContent>
                        <CardActions className={classes.actions} disableActionSpacing>
                            <IconButton onClick={favorite} aria-label="Add to favorites">
                                {favorited ? <FavoriteIcon /> : <FavoriteBorderIcon />}
                            </IconButton>
                        </CardActions>
                    </Card>
                    : ''}
            </main>
        </div>
    )
}

Detail.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Detail);