import React, { useState, useEffect } from "react";
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/core/styles/index';

import Showroom from '../../components/showroom';
import Sidebar from '../../components/sidebar';

const styles = theme => ({
    root: {
        flexGrow: 1,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    drawerPaper: {
        position: 'relative',
        width: 240,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
    },
    toolbar: theme.mixins.toolbar,
});

export const Home = (props) => {
    const { classes } = props;
    const [initialized, setInitialized] = useState(false);
    const [fetechdBeers, setFetechdBeers] = useState();
    const [beers, setBeers] = useState();

    useEffect(() => {
        if (!initialized) {
            const apiKey = 'bbd8e9f800b52500d7a3e1528c032250';
            fetch('/v2/beers/?key=' + apiKey)
                .then((response) => response.json())
                .then((responseJson) => {
                    setFetechdBeers(responseJson.data)
                    setBeers(randomBeers(responseJson.data, 10))
                })
            setInitialized(true);
        }
    }, []);

    const randomBeers = (beers, items) => {
        let randomIndexs = []
        while (randomIndexs.length < items) {
            const index = Math.floor(Math.random() * 50) + 1;
            if (randomIndexs.indexOf(index) === -1) randomIndexs.push(index);
        }

        return beers.filter((beer, index) => randomIndexs.indexOf(index) >= 0)
    };

    const onSidebarRandom = () => {
        setBeers(randomBeers(fetechdBeers, 10))
    };

    return (
        <div className={classes.root}>
            <AppBar position="absolute" className={classes.appBar}>
                <Toolbar>
                    <Typography variant="title" color="inherit" noWrap>
                        Beers Showroom
                    </Typography>
                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <Sidebar onSidebarRandom={onSidebarRandom} />
            </Drawer>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                {beers ? <Showroom beers={beers} /> : <Typography align={'center'} variant="title">Loading...</Typography>}
            </main>
        </div>
    )
}

Home.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Home);
