import React, { useState, useEffect } from "react";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles/index';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Link } from "react-router-dom";

import Showroom from '../../components/showroom';

const styles = theme => ({
    root: {
        flexGrow: 1,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
    },
    toolbar: theme.mixins.toolbar,
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
        color: '#ffffff'
    },
});

const Favorites = (props) => {
    const { classes } = props;
    const [initialized, setInitialized] = useState(false);
    const [beers, setBeers] = useState();
    const [favorites] = useState(localStorage.getItem('favorites') ? JSON.parse(localStorage.getItem('favorites')) : []);

    useEffect(() => {
        if (!initialized) {
            let fethQueue = [];
            favorites.forEach(beerId => {
                const apiKey = 'bbd8e9f800b52500d7a3e1528c032250';
                fethQueue.push(fetch(`/v2/beer/${beerId}/?key=${apiKey}`)
                    .then((response) => response.json())
                    .then((responseJson) => {
                        return responseJson.data
                    }))
            });
            Promise.all(fethQueue).then((beers) => {
                setBeers(beers)
            })

            setInitialized(true);
        }
    }, []);

    return (
        <div className={classes.root}>
            <AppBar position="absolute" className={classes.appBar}>
                <Toolbar>
                    <Link to={'/'}>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                            <ArrowBackIcon />
                        </IconButton>
                    </Link>
                    <Typography variant="title" color="inherit" noWrap>
                        Beers Showroom
                    </Typography>
                </Toolbar>
            </AppBar>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                {beers ? <Showroom beers={beers} /> : <Typography align={'center'} variant="title">Loading...</Typography>}
            </main>
        </div>
    )
}

Favorites.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Favorites);