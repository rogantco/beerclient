import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom";

import Home from './scenes/Home';
import Detail from './scenes/Detail';
import Favorites from './scenes/Favorites';

class App extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/beer/:id" component={Detail} />
                <Route path="/favorites" component={Favorites} />
            </Switch>
        );
    }
}

export default App;