import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import { Link } from "react-router-dom";

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: '100%',
        height: '100%',
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
});

const Showroom = (props) => {
    const { classes, beers } = props;

    return (
        <div className={classes.root}>
            <GridList cellHeight={300} className={classes.gridList}>
                {beers.map(beer => (
                    <GridListTile key={beer.id}>
                        <img src={beer.labels ? beer.labels.medium : 'no-thumbnail.png'} alt={beer.title} />
                        <Link to={`/beer/${beer.id}`}>
                            <GridListTileBar
                                title={beer.name}
                                subtitle={<span>{beer.description}</span>}
                                actionIcon={                                    
                                    <IconButton className={classes.icon}>
                                        <InfoIcon />
                                    </IconButton>  
                                }
                            />
                        </Link>
                    </GridListTile>
                ))}
            </GridList>
        </div>
    );
}

Showroom.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Showroom);