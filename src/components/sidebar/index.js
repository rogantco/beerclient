import React from 'react';
import PropTypes from 'prop-types';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import LoopIcon from '@material-ui/icons/Loop';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { Link } from "react-router-dom";

const styles = theme => ({
    root: {
        width: '100%',
    },
    toolbar: theme.mixins.toolbar,
    link: {
        color: 'inherit',
        textDecoration: 'none'
    }
});

const menuClickHandler = (onSidebarRandom) => {
    onSidebarRandom();
}

const Sidebar = (props) => {
    const { classes, onSidebarRandom } = props;

    return (
        <div>
            <div className={classes.toolbar} />
            <MenuList>
                <MenuItem onClick={menuClickHandler.bind(this, onSidebarRandom)}>
                    <ListItemIcon>
                        <LoopIcon />
                    </ListItemIcon>
                    <Typography variant="inherit">More Random Beers</Typography>
                </MenuItem>
                <MenuItem>
                    <ListItemIcon>
                        <FavoriteIcon />
                    </ListItemIcon>
                    <Typography variant="inherit">
                        <Link className={classes.link} to="/favorites">Favorites</Link>
                    </Typography>
                </MenuItem>
            </MenuList>
        </div>
    );
}

Sidebar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Sidebar);
