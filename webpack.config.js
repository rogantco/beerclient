module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["env", "react"],
                      }
                }
            }
        ],
    },
    devServer: {
        contentBase: './dist',
        open: true,
        historyApiFallback: true,
        proxy: {
            '/v2': {
                target: 'https://sandbox-api.brewerydb.com',
                secure: false,
                changeOrigin: true
            }
        }
    }
};